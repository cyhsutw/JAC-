require 'json'
require 'dalli'
require 'sinatra'
require 'httparty'
require 'rack-flash'
require 'rack/ssl-enforcer'
require_relative 'model/user.rb'
require_relative 'helpers/credit_card_web_app_helper.rb'

# credit card api service
class CreditCardWebApp < Sinatra::Base
  include CreditCardWebAppHelper

  enable :logging

  configure :development, :test do
    file = File.new("#{settings.root}/log/#{settings.environment}.log", 'a+')
    file.sync = true
    use Rack::CommonLogger, file
  end

  configure do
    use Rack::SslEnforcer
    use Rack::Session::Cookie, secret: ENV['MSG_KEY']
    use Rack::Flash, sweep: true
    set :cc_cache, Dalli::Client.new((ENV['MEMCACHIER_SERVERS'] || '').split(','),
                                     username: ENV['MEMCACHIER_USERNAME'],
                                     password: ENV['MEMCACHIER_PASSWORD'],
                                     socket_timeout: 1.5,
                                     socket_failure_delay: 0.2)
  end

  before do
    @current_user = find_user_by_token(session[:auth_token])
  end

  get '/' do
    if @current_user
      @cards = cc_cache.get(@current_user.id.to_s)
      if @cards.nil?
        @cards = fetch_cc
      else
        @cards = JSON.parse(@cards)
      end
    end
    haml :index
  end

  get '/sign_up' do
    if params.has_key? 'token'
      begin
        create_user_with_token(params[:token])
        flash[:notice] = 'Welcome! Your account has been successfully created.'
      rescue
        flash[:error] = 'Your account could not be created. Your link is either expired or invalid.'
      end
      redirect '/'
    else
      haml :sign_up
    end
  end

  post '/logout' do
    session[:auth_token] = nil
    flash[:notice] = 'You have logged out'
    redirect '/'
  end

  post '/sign_up' do
    if params[:password] == params[:password_confirm]
      begin
        user = User.new(params.except(:password_confirm.to_s))
        user.password = params[:password]
        unless user.validate
          flash[:error] = 'Please fill in all the fields and make sure passwords match'
          redirect '/sign_up'
          halt
        end
        send_activation_email_with_payload(params.except(:password_confirm.to_s))
        flash[:notice] = 'A verification link has been sent to you. Please check your email!'
        redirect '/'
      rescue => e
        logger.error "FAIL EMAIL: #{e}"
        flash[:error] = 'Could not send registration verification: check email address'
        redirect '/sign_up'
      end
    else
      flash[:error] = 'Please fill in all the fields and make sure passwords match'
      redirect '/sign_up'
      halt
    end
  end

  get '/sign_in/?' do
    haml :sign_in
  end

  post '/sign_in/?' do
    username = params[:username]
    password = params[:password]
    user = User.authenticate!(username, password)
    if user.present?
      login(user)
    else
      flash[:error] = 'Error, please check your username and password!'
      redirect('/sign_in/')
    end
  end

  # OAuth
  get '/oauth/?' do
    unless params[:code].present?
      flash[:error] = 'Can not find authorization code, please try again later!'
      redirect '/'
      halt
    end

    payload = {
      client_id: ENV['GH_CLIENT_ID'],
      client_secret: ENV['GH_CLIENT_SECRET'],
      code: params[:code]
    }
    token_resp = HTTParty.post('https://github.com/login/oauth/access_token',
                               headers: { 'Accept' => 'application/json' }, body: payload)

    unless token_resp['access_token'].present?
      flash[:error] = 'Authorization error, please try again later!'
      redirect '/'
      halt
    end

    payload = { params: { access_token: token_resp['access_token'] } }

    user_resp = HTTParty.get('https://api.github.com/user',
                             body: payload,
                             headers: {
                               'User-Agent' => 'Credit_Card_API/1.0',
                               'Authorization' => "token #{token_resp['access_token']}"
                             })

    if (user = User.find_by(username: user_resp['login'])).present?
      login(user)
    else
      create_gh_user(user_resp, token_resp['access_token'])
    end
  end


  get '/credit_cards/new/?' do
    if @current_user
      haml :new_credit_card
    else
      flash[:error] = 'INVISIBLE PAGE'
      redirect('/')
    end
  end

  post '/credit_cards/?' do
    if @current_user
      req_url = ENV['API_SERVER_ADDRESS'] + '/api/v1/credit_card?user_id=' + @current_user.id.to_s
      resp = HTTParty.post(req_url, body: params.to_json, headers: auth_headers)
      if resp.code == 201
        flash[:notice] = 'Successfully created...'
      else
        flash[:error] = 'Error creating credit card.'
      end
    else
      flash[:error] = 'Unauthorized'
    end
    redirect '/'
  end

  get '/credit_cards/?' do
    if @current_user
      @credit_cards = fetch_cc
      if @credit_cards.nil?
        flash[:error] = 'Error listing credit cards'
        redirect '/'
      else
        haml :credit_cards
      end
    else
      flash[:error] = 'Unauthorized'
      redirect '/'
    end
  end
end

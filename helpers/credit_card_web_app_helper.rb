require 'rbnacl/libsodium'
require 'jwt'
require 'pony'
require 'base64'

module CreditCardWebAppHelper

  def send_activation_email_with_payload(payload)
    jwt = JWT.encode payload, ENV['MSG_KEY'], 'HS256'
    token = encrypt_token(jwt)
    Pony.mail(to: payload[:email],
              subject: 'Your Credit Card App Account is Ready',
              html_body: activation_email_with_token(token))
  end

  def activation_email_with_token(token)
    <<-MAIL_BODY
      <h1>Credit Card App API registration</h1>
      <p>
        Please
        <a href="#{request.base_url}/sign_up?token=#{token}">
          click here
        </a>
         to validate your email and activate your account.
      </p>
    MAIL_BODY
  end

  def secret_box
    @s_box = RbNaCl::SecretBox.new(Base64.urlsafe_decode64(ENV['MSG_KEY']))
  end

  def encrypt_token(msg)
    nonce = RbNaCl::Random.random_bytes(secret_box.nonce_bytes)
    json_payload = {
      message: Base64.urlsafe_encode64(secret_box.encrypt(nonce, msg.to_s)),
      nonce: Base64.urlsafe_encode64(nonce)
    }.to_json
    Base64.urlsafe_encode64(json_payload)
  end

  def decrypt_token(msg)
    payload = JSON.parse(Base64.urlsafe_decode64(msg))
    nonce = Base64.urlsafe_decode64(payload['nonce'])
    cipher = Base64.urlsafe_decode64(payload['message'])
    JWT.decode(secret_box.decrypt(nonce, cipher), ENV['MSG_KEY'], 'HS256').first
  end

  def create_user_with_token(token)
    payload = decrypt_token(token)
    user = User.new(payload)
    user.password = payload['password']
    user.save ? login(user) : fail('Can not create user')
  end

  def login(user)
    payload = { user_id: user.id }
    token = JWT.encode payload, ENV['MSG_KEY'], 'HS256'
    session[:auth_token] = token
    redirect '/'
  end

  def find_user_by_token(token)
    return nil unless token
    decoded_token = JWT.decode token, ENV['MSG_KEY'], true
    payload = decoded_token.first
    logger.info "PAYLOAD: #{payload}"
    User.find_by_id(payload["user_id"])
  end

  def user_jwt
    jwt_key = OpenSSL::PKey::RSA.new(Base64.urlsafe_decode64(ENV['RSA_PRI_KEY']))
    jwt_payload = { iss: request.base_url, sub: @current_user.id }
    JWT.encode(jwt_payload, jwt_key, 'RS256')
  end

  def auth_headers
    { 'authorization' => ('Bearer ' + user_jwt) }
  end

  # OAuth
  def create_gh_user(payload, access_token)
    # handle private emails
    unless (email = payload['email']).present?
      emails = HTTParty.get('https://api.github.com/user/emails',
                            headers: {
                              'User-Agent' => 'Credit_Card_API/1.0',
                              'Authorization' => "token #{access_token}"
                            })
      email = emails.first['email']
    end
    user = User.new(username: payload['login'],
                    email: email,
                    fullname: payload['name'])
    user.password = access_token
    user.save ? login(user) : fail('Can not create user')
  end

  # cache
  def cc_cache
    settings.cc_cache
  end

  def fetch_cc
    req_url = ENV['API_SERVER_ADDRESS'] + '/api/v1/credit_card?user_id=' + @current_user.id.to_s
    resp = HTTParty.get(req_url, headers: auth_headers)
    resp.code == 200 ? JSON.parse(resp.body) : nil
  end

end
